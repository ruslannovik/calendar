const TIMER_INTERVAL = 1000;
const MONTHS = 12;
const WEEK_DAYS = 7;
const SAT = 5;
const SUN = 6;
const ENTER_KEYCODE = 13;

const timeDisplay = document.querySelector('.time__display');
const timeHours = document.querySelector('.time__hours');
const timeMinutes = document.querySelector('.time__minutes');
const addingText = document.querySelector('.adding__text');
const addEventBtn = document.querySelector('.adding__btn');

let chosenDay = new Date();
let calendarEvents = getFromStorage();
let isEditing;

timeHours.addEventListener('change', changeTimeHandler);
timeMinutes.addEventListener('change', changeTimeHandler);
addEventBtn.addEventListener('click', addCalendarEvent) 

printCalendar(chosenDay);
setInterval(printCurrentDate, TIMER_INTERVAL);

function printCalendar(date) {
    printCalendarYears(date);
    printCalendarMonths(date);
    printCalendarEvents();
    printEventsTime();
}

function printCalendarYears(data) {
    const yearDiv = document.querySelector('.calendar__years-current');
    const previousYearDiv = document.querySelector('.calendar__years-previous');
    const nextYearDiv = document.querySelector('.calendar__years-next');

    yearDiv.innerText = data.getFullYear();
    previousYearDiv.addEventListener('click', printPreviousYear);
    nextYearDiv.addEventListener('click', printNextYear);
}

function printPreviousYear() {
    chosenDay.setFullYear(chosenDay.getFullYear() - 1);
    printCalendar(chosenDay);
}

function printNextYear() {
    chosenDay.setFullYear(chosenDay.getFullYear() + 1);
    printCalendar(chosenDay);
}

function printCalendarMonths(date) {
    const monthsDiv = document.querySelector('.calendar__months');

    monthsDiv.innerHTML = '';
    for (let i = 0; i < MONTHS; i++) {
        monthsDiv.append(createMonth(date, i));
    }
}

function printCalendarEvents() {
    const calendarEventsDiv = document.querySelector('.calendar__events');

    sortCalendarEvents();
    calendarEventsDiv.innerHTML = '';
    calendarEvents.forEach(item => {
        if (getBeginnigDay(item.date) === getBeginnigDay(chosenDay)) {
            calendarEventsDiv.append(createCalendarEventDiv(item));
        }
    });
    scrollToLastElemOfEvents(calendarEventsDiv);
}

function printEventsTime() {
    timeHours.value = chosenDay.getHours();
    timeMinutes.value = chosenDay.getMinutes();
    timeDisplay.innerText = getTime(chosenDay);
}

function changeTimeHandler() {
    chosenDay.setHours(timeHours.value);
    chosenDay.setMinutes(timeMinutes.value);
    timeDisplay.innerText = getTime(chosenDay);
}

function createMonth(date, month) {
    const monthDiv = createElemWithClasses('div', 'month');

    createdMonth = new Date(date.getFullYear(), month);
    monthDiv.append(createMonthTitle(createdMonth));
    monthDiv.append(createMonthHead());
    while (createdMonth.getMonth() === month) {
        monthDiv.append(createWeek(createdMonth, month));
    }

    return monthDiv;
}

function createMonthTitle(date) {
    const titleMonth = createElemWithClasses('div', 'month__title');

    titleMonth.innerText = getMonthName(date.getMonth());

    return titleMonth;
}

function getMonthName(value) {
    const months = [
            'январь', 'февраль', 'март', 'апрель',
            'май', 'июнь', 'июль', 'август', 
            'сентябрь', 'октябрь', 'ноябрь', 'декабрь'
        ];

    return months[value];
}

function getFullDate(data) {
    const date = addZero(data.getDate());
    const month = addZero(data.getMonth() + 1);
    const year = addZero(data.getFullYear());

    return `${date}.${month}.${year}`;
}

function getFullTime(data) {
    const timeWithoutSeconds = getTime(data);
    const seconds = addZero(data.getSeconds());

    return `${timeWithoutSeconds}:${seconds}`;
}

function getTime(data) {
    const hours = addZero(data.getHours());
    const minutes = addZero(data.getMinutes());

    return `${hours}:${minutes}`;
}

function addZero(data) {
    return data < 10 ? '0' + data : data;
}

function createMonthHead() {
    const headMonth = createElemWithClasses('div', 'month__head');
    const days = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];

    days.forEach(item => {
        const dayDiv = document.createElement('div');

        dayDiv.classList.add('day');
        dayDiv.innerText = item;
        headMonth.append(dayDiv);
    });

    return headMonth;
}

function createWeek(date, month) {
    const week = createElemWithClasses('div', 'week');

    fillFirstWeedByEmptyDay(week, date);
    while (date.getMonth() === month) {
        week.append(createDay(date));
        if (getDay(date) % WEEK_DAYS === SUN) {
            incrementDay(date);
            return week;
        } 

        incrementDay(date);
    }

    fillLastWeedByEmptyDay(week, date);

    return week;
}

function fillFirstWeedByEmptyDay(week, date) {
    for (let i = 0, n = getDay(date); i < n; i++){
        week.append(createEmptyDay());
    }
}

function fillLastWeedByEmptyDay(week, date) {
    for (let i = getDay(date); i < WEEK_DAYS; i++) {
        week.append(createEmptyDay());
    }
}

function createDay(date) {
    const day = createElemWithClasses('div', 'day');

    addEventClass(date, day);
    addWeekendClass(date, day);
    addChosenClass(date, day);
    switchDayBehavior(day);
    day.classList.add('day_full');
    day.innerText = date.getDate();
    day.id = +date;
    day.addEventListener('click', chooseDay);
    
    return day;
}

function addEventClass(date, day) {
    const isEvent = calendarEvents.some(item => {
        return getBeginnigDay(item.date) === getBeginnigDay(date);
    });

    if (isEvent) day.classList.add('day_event')
}

function addWeekendClass(date, day) {
    if (getDay(date) === SAT || getDay(date) === SUN) {
        day.classList.add('day_weekend');
    }
}

function addChosenClass(date, day) {
    if (getBeginnigDay(chosenDay) === getBeginnigDay(date)) {
        day.classList.add('day_chosen');
     }
}

function switchDayBehavior(day) {
    if (isEditing) {
        day.classList.add('day_editing');
        day.addEventListener('click', changeDate);
    }
}

function chooseDay(event) {
    chosenDay = new Date(+event.target.id);
    chosenDay.setHours(timeHours.value);
    chosenDay.setMinutes(timeMinutes.value);
    printCalendar(chosenDay);
}

function changeDate(event) {
    const changedEvent = calendarEvents.find(item => item.editState);

    changedEvent.date = new Date(+event.target.id);
    changedEvent.date.setHours(chosenDay.getHours());
    changedEvent.date.setMinutes(chosenDay.getMinutes());
    changedEvent.editState = false;
    isEditing = false;
    saveInStorage(calendarEvents);
    printCalendar(chosenDay);
}

function createEmptyDay() {
    const day = createElemWithClasses('div', 'day');

    return day;
}

function createCalendarEventDiv(calendarEvent) {
    const eventDiv = createElemWithClasses('div', 'event');
    const date = createEventDateDiv(calendarEvent);
    const text = createEventTextDiv(calendarEvent);
    const btn = createCloseBtn(calendarEvent);

    eventDiv.id = calendarEvent.id;
    eventDiv.append(date, text, btn);

    return eventDiv;
}

function createEventDateDiv(calendarEvent) {
    const date = createElemWithClasses('input', 'event__date');

    if (!isEditing) date.addEventListener('click', dateEditHandler);

    if (calendarEvent.editState) {
        date.value = 'установите дату и время';
        date.disabled = true;
    } else {
        const dateAndTime = getFullDate(calendarEvent.date) + ' ' + getTime(calendarEvent.date);
        date.value = dateAndTime;
    }

    return date;
}

function createEventTextDiv(calendarEvent) {
    const text = createElemWithClasses('input', 'event__text');

    text.value = calendarEvent.text;
    text.addEventListener('click', textEditHandler);

    return text;
}

function createCloseBtn(calendarEvent) {
    const btn = createElemWithClasses('div', 'event__close');

    btn.innerText = 'x';
    btn.onclick = () => deleteCalendarEvent(calendarEvent);

    return btn;
}

function dateEditHandler(event) {
    const id = event.target.parentNode.id;
    const calendarEvent = calendarEvents.find(item => item.id === id);

    isEditing = true;
    calendarEvent.editState = true;
    printCalendar(chosenDay);
}

function textEditHandler(event) {
    event.target.addEventListener('keydown', saveChangedText);
    event.target.addEventListener('blur', saveChangedText);
}

function saveChangedText(event) {
    if (event.keyCode === ENTER_KEYCODE || event.type === 'blur' ) {
        const id = event.target.parentNode.id;
        const calendarEvent = calendarEvents.find(item => item.id === id);

        calendarEvent.text = event.target.value;
        if (event.type !== 'blur') event.target.blur();
        saveInStorage(calendarEvents);
    }
}

function deleteCalendarEvent(event) {
    calendarEvents = calendarEvents.filter(item => item.id !== event.id);
    saveInStorage(calendarEvents);
    printCalendarEvents();
}

function createCalendarEvent() {
    const id = generateId();
    const date = new Date(chosenDay);
    const text = addingText.value;
    const editState = false;

    return {
        id,
        date,
        text,
        editState,
    }
}

function generateId() {
    return String(calendarEvents.reduce((max, item) => Math.max(max, item.id), 0) + 1);
}

function incrementDay(date) {
    date.setDate(date.getDate() + 1);
}

function createElemWithClasses(elem, ...classes) {
    const createdElem = document.createElement(elem);

    classes.forEach(item => {
        createdElem.classList.add(item);
    });

    return createdElem;
}

function getDay(date) {
    let day = date.getDay();

    if (day === 0) day = WEEK_DAYS;

    return day - 1;
}

function getBeginnigDay(data) {
    const curTime = new Date(+data);
    const dayStart = curTime.setHours(0,0,0,0);

    return dayStart;
}

function addCalendarEvent() {
    calendarEvents.push(createCalendarEvent());
    addingText.value = '';
    saveInStorage(calendarEvents);
    printCalendar(chosenDay);
}

function sortCalendarEvents() {
    calendarEvents.sort((a, b) => a.date - b.date);
}

function scrollToLastElemOfEvents(calendarEventsDiv) {
    const calendarEventsArr = calendarEventsDiv.children;
    const lastElem = calendarEventsArr[calendarEventsArr.length - 1];

    if (lastElem) {
        lastElem.scrollIntoView({behavior: 'smooth'});
    } 
}

function saveInStorage(calendarEvents) {
    const str = JSON.stringify(calendarEvents);

    localStorage.setItem('calendarEvents', str);
}

function getFromStorage() {
    const data = localStorage.getItem('calendarEvents');
    const calendarEvents = data ? JSON.parse(data, convertStringToDate) : [];

    return calendarEvents;
}

function convertStringToDate(key, value) {
    if (key == 'date') return new Date(value);

    return value;
}

function printCurrentDate() {
    const display = document.querySelector('.watch__display');
    const date = new Date();
    const dateStr = getFullDate(date) + ' ' + getFullTime(date);
    display.innerText = dateStr;
}
